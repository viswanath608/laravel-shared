<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>{{$title}}</title>
	
	<!-- stylesheets -->
	<link href="/assets/stylesheets/foundation.min.css" rel="stylesheet" />

	<!-- Javascript -->
	<script src="/assets/javascripts/modernizr.foundation.js"></script>

</head>
<body>
	<div class="row">
		<div class="twelve columns">
			<h1>{{$title}}</h1>
			<hr>

			@if (Session::has('message'))
				<div class="alert-message success">
					<p>{{Session::get('message')}}</p>
				</div>
			@endif

			@if($errors->has())
				<div class="alert-message error">
					@foreach($errors->all('<p>:message</p>') as $error)
						{{$error}}
					@endforeach
				</div>
			@endif
		</div>
		<div class="twelve columns">
			{{$content}}
		</div>
	</div>
	<!-- Heavy scripts -->
	<script src="/assets/javascripts/jquery.js"></script>
	<script src="/assets/javascripts/jquery.placeholder.js"></script>
	<script src="/assets/javascripts/foundation.min.js"></script>
	<script src="/assets/javascripts/app.js"></script>
</body>
</html>
