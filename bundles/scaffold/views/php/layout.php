<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title><?php echo '<?php'; ?> echo $title; ?></title>
	
	<!-- stylesheets -->
	<link href="/assets/stylesheets/foundation.min.css" rel="stylesheet" />

	<!-- Javascript -->
	<script src="/assets/javascripts/modernizr.foundation.js"></script>
</head>
<body>
	<div class="row">
		<div class="twelve columns">
			<h1><?php echo '<?php'; ?> echo $title; ?></h1>
			<hr>

			<?php echo '<?php'; ?> if (Session::has('message')): ?>
				<div class="alert-message success">
					<p><?php echo '<?php'; ?> echo Session::get('message'); ?></p>
				</div>
			<?php echo '<?php endif; ?>'.PHP_EOL; ?>

			<?php echo '<?php'; ?> if($errors->has()): ?>
				<div class="alert-message error">
					<?php echo '<?php '; ?> foreach($errors->all('<p>:message</p>') as $error): ?>
						<?php echo '<?php'; ?> echo $error; ?>
					<?php echo '<?php endforeach; ?>'.PHP_EOL; ?>
				</div>
			<?php echo '<?php endif; ?>'.PHP_EOL; ?>
		</div>
		<div class="twelve columns">
			<?php echo '<?php '; ?> echo $content; ?>
		</div>
	</div>

	<!-- Heavy scripts -->
	<script src="/assets/javascripts/jquery.js"></script>
	<script src="/assets/javascripts/jquery.placeholder.js"></script>
	<script src="/assets/javascripts/foundation.min.js"></script>
	<script src="/assets/javascripts/app.js"></script>
</body>
</html>
