laravel-shared
==============

Laravel 3.2.13 for shared hosting, with additonal modules for easy and faster development of your app :).

License: All the license and copyrights belongs to respected creators.

My contribution: Edited few lines in scaffold bundle, authority and laravel application/*.
