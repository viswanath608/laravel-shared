<!DOCTYPE html>
<html >
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<title>Laravel</title>

		<!-- stylesheets -->
		<link href="/assets/stylesheets/foundation.min.css" rel="stylesheet" />

		<!-- Javascript -->
		<script src="/assets/javascripts/modernizr.foundation.js"></script>
	</head>
	<body>

		@yield('content')
		
		<!-- Heavy scripts -->
		<script src="/assets/javascripts/jquery.js"></script>
		<script src="/assets/javascripts/jquery.placeholder.js"></script>
		<script src="/assets/javascripts/foundation.min.js"></script>
		<script src="/assets/javascripts/app.js"></script>
	</body>
</html>